# Movie-DB #

## How to get Movie-DB ##

```bash
git clone https://github.com/JMazurkiewicz/Baza-Filmowa-PRI.git
cd Baza-Filmowa-PRI

mkdir build
cd build
cmake ..
cmake --build .
```

## Project requirements ##

Write a database that stores a database of movies, actors and recording studios. The base should allow:

* adding and removing movies, actors and recording studios
* sorting movies, actors and recording studios according to various criteria
* listing of film data: recording studio making a film, list of actors
* listing of data about the recording studio: name, address of the registered office, contact, list of produced films, list of actors with whom the given recording studio cooperates according to the film
* listing of actor data: name and surname, list of films in which the actor participated
* actors can appear in several films
* a movie can have several actors and one recording studio
* one recording studio makes several films

Use dynamic memory allocation (make sure it is properly freed and no memory leaks). Used structures (list, tree) and sorting should be implemented by yourself. The program should be protected against attempts to enter incorrect input data. Please also provide a user-friendly interface - random ‘Kowalski’ should be able to use this program and have access to the instructions. The code should be readable and divided into `.h` and` .c` files. Make sure the code is divided into functions.

Other requirements:

* saving the database to files
* reading the database from recently created files (restoring from backup if the program is turned off) - give the user a choice between the latest and slightly older record based on timestamp
* when reading the database from files, display the date of the last backup file modification
* deleting backup files

You should also take care of handling file errors, situations when the file will be damaged and the record will be incomplete.